﻿namespace RiderExtractMethodBug;

public static class Program
{
    public static void Main()
    {
        // Select the two lines below, and try extract method:
        const int colorId = Colors.Blue;
        bool isBlue = colorId == 1;

        // Note: it works if the colorName above is NOT const.
        /* I expect something like this (as it is shown in the extract method preview window):
         private static bool IsBlue(string colorName)
        {
            const string colorName = Colors.Blue;
            bool isBlue = colorName == "blue";
            return isBlue;
        }
         */

        if (isBlue)
        {
            Console.WriteLine("it is blue");
        }
    }
}

public static class Colors
{
    public const int Blue = 1;
}
